Views Package
=============
A simple view renderer. Will return the view from an external file, bind variables to 
it, and either render or return it.

Options
-------

The following options are avaliable during object construction:

| Option       | Description                                      | Required           |
|:-------------|:-------------------------------------------------|:-------------------|
| views.dir    | Root path of your views directory.               | Yes                |
| views.return | Return views as string on True. Output on False. | No, Default: false |
| views.header | Location of a header template.                   | No                 |
| views.footer | Location of footer template.                     | No                 |