<?php namespace WesternNevadaCollege\Views;

use WesternNevadaCollege\Views\Exceptions\NotFound;

class View 
{
    /**
     * Class options.
     * 
     * @var array 
     */
    protected $options = array();
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Constructor.
     * 
     * @param array $options See README.md
     * @throws \LogicException If required options are missing.
     */
    public function __construct($options)
    {
        if ( ! array_key_exists('views.dir', $options))
        {
            throw new \LogicException("Must pass 'views.dir' option.");
        }
        
        $this->options = $options;
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Render views.
     * 
     * Renders the desired view and passes any variables to it.
     * 
     * @param string $view
     * @param array $vars array('var' => 'value') be passed to view as $var = $value
     * @return boolean
     * @throws NotFound If view is not found.
     */
    public function render($view, $vars = array())
    {        
        // Prefixed with an under score to prevent name collision below.
        $_file = str_replace('.', '/', $view);
        $_file = $view . '.phtml';
        
        $_file = rtrim($this->options['views.dir'], '/') . '/' . $_file;
        
        if ( ! \file_exists($_file))
        {
            throw new NotFound('View not found - ' . $view);
        }
        unset($view); // Unsetting to prevent name collision.
        
        \ob_start();
        
        // Pass variables to view scope.
        $_vars = $vars;
        unset($vars);
        foreach ($_vars as $_var => $_value)
        {
            $$_var = $_value;
        }
        unset($_vars, $_var, $_value);
        
        $this->attemptTemplateFileLoad('views.header');
        require $_file;
        $this->attemptTemplateFileLoad('views.footer');
        
        if ($this->checkOption('views.return', false)) 
        {
            return \ob_get_clean();
        }
        else
        {
            echo \ob_get_clean();
        }
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Attempt to load template file.
     * 
     * @param string $element
     * @throws \LogicException
     */
    protected function attemptTemplateFileLoad($element)
    {
        if (isset($this->options[$element]))
        {
            $file = $this->options[$element];
            if ( ! file_exists($file))
            {
                throw new \LogicException(ucfirst($element) . ' file not found. File - ' . $file);
            }
            
            require $this->options[$element];
        }
    }
    
    // ---------------------------------------------------------------------------------------------
    
    /**
     * Option checking.
     * 
     * @param string $element
     * @param mixed $default
     * @return type
     */
    protected function checkOption($element, $default = null)
    {
        return isset($this->options[$element]) ? $this->options[$element] : $default;
    }
}